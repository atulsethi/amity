package com.app.amity.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.app.amity.model.DataItem

@Database(entities = [DataItem::class], version = 1)
abstract class DataDatabase : RoomDatabase() {

    abstract fun dataDao(): DataDao
}