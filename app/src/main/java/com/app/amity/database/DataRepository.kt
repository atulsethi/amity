package com.app.amity.database

import com.app.amity.model.DataItem
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers


class DataRepository(private  var mDataDao: DataDao) :DataRepositoryInterface {
    override fun saveAll(dataItemList: List<DataItem>) {

        Observable.just(true)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
             mDataDao.saveAll(dataItemList)
            }

    }

    override fun getAll(): Observable<List<DataItem>> {
      return    mDataDao.getAllData()
    }

}