package com.app.amity.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.app.amity.model.DataItem
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface DataDao {

    @Insert
    fun saveAll(entities: List<DataItem>)


    @Query("SELECT * FROM dataitem WHERE id = :id")
    fun findDataById(id: String): Single<DataItem>


    @Query("SELECT * FROM dataitem ORDER BY id DESC")
    fun getAllData(): Observable<List<DataItem>>

}