package com.app.amity.database

import androidx.room.Room
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.Module
import org.koin.dsl.module


val mDatabaseModule: Module = module {
    single {
        Room.databaseBuilder(androidApplication(), DataDatabase::class.java, "data-db")
            .allowMainThreadQueries().build()
    }

    single { DataRepository(get()) }

    single {
        get<DataDatabase>().dataDao()
    }


}

