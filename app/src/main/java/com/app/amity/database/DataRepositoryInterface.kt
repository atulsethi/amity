package com.app.amity.database

import com.app.amity.model.DataItem
import io.reactivex.Observable

interface DataRepositoryInterface {
    fun  saveAll( dataItemList:List<DataItem>)
    fun getAll() : Observable<List<DataItem>>

}