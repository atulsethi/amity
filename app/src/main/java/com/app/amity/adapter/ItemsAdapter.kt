package com.app.amity.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.amity.R
import com.app.amity.model.DataItem

class ItemsAdapter(private val listener: OnItemClickListener) :
    RecyclerView.Adapter<ItemsAdapter.ViewHolder>() {
    private var dataItemList: List<DataItem>? = null

    interface OnItemClickListener {
        fun onItemClick(pos: DataItem?)
    }

  public  fun setData(mItemsList: List<DataItem>?) {
        dataItemList = mItemsList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_view_data, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        val mDataItem = dataItemList!![position]
        holder.bind(mDataItem)
    }

    override fun getItemCount(): Int {
        return if (dataItemList != null) {
            dataItemList!!.size
        } else 0
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private val tvItemTittle: TextView = itemView.findViewById(R.id.tvTitle)
        private val tvItemStatus: TextView = itemView.findViewById(R.id.tvStatus)

        fun bind(pos: DataItem) {
            tvItemTittle.text = StringBuilder()
                .append(pos.title)
            tvItemStatus.text = StringBuilder()
                .append(pos.completed)
            itemView.setOnClickListener { view: View? ->
                listener.onItemClick(
                    pos
                )
            }
        }

    }

}