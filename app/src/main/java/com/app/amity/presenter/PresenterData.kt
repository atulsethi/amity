package com.app.amity.presenter

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.app.amity.database.DataRepository
import com.app.amity.model.DataItem
import com.app.amity.restClient.GetAllDataApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.inject

class PresenterData(var dataList:MutableLiveData<List<DataItem>>) : KoinComponent {


    private val mDataApi: GetAllDataApi by inject()

    private val mDataRepository: DataRepository by inject()

    init {


        mDataRepository.getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { it ->

                    if(it.isEmpty())
                    {
                        callApi()
                    }else
                    {


                        dataList.value=it

                    }


                },
                { error -> Log.d("RxJava", "Error getting info from interactor into presenter") }
            )



    }


    private  fun callApi()
    {

        mDataApi.getData()  .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { it ->

                    dataList.value=it

                    mDataRepository.saveAll(it)






                },
                { e ->

                    e.printStackTrace()

                })

    }



}