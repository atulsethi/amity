package com.app.amity.restClient

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.URL
import java.util.concurrent.TimeUnit

fun createNetworkClient(baseUrl: String) = retrofitClient(baseUrl)

var gson = GsonBuilder()
    .setLenient()
    .create()!!

var httpClient= getClient()



fun getClient(): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    var builder = OkHttpClient.Builder()
    builder.followRedirects(true)
        .followSslRedirects(true)
        .addNetworkInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                System.out.println(" chain url: " + chain.request().url())
                return chain.proceed(chain.request())
            }
        })
    //builder = SecurityUtils.AddTrustManager(builder, serverHostname)
    // Stop Log
    builder.interceptors().add(httpLoggingInterceptor)
    builder.readTimeout(60, TimeUnit.SECONDS)
    builder.connectTimeout(60, TimeUnit.SECONDS)
    return builder.build()
}



private fun retrofitClient(baseUrl: String): Retrofit =
    Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(httpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

