package com.app.amity.restClient

import com.app.amity.model.Data
import io.reactivex.Single
import retrofit2.http.GET


interface GetAllDataApi {
    @GET("todos")
    fun getData(): Single<Data>
}

