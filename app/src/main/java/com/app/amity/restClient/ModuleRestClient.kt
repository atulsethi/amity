package com.app.amity.restClient

import android.util.Log
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import java.net.URL
import java.util.concurrent.TimeUnit


val networkModule: Module = module {
    single { mDataApi }
}

private const val BASE_URL = "https://jsonplaceholder.typicode.com/"

private val retrofit: Retrofit = createNetworkClient(BASE_URL)

private val mDataApi: GetAllDataApi = retrofit.create(GetAllDataApi::class.java)

