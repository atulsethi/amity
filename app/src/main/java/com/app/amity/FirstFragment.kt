package com.app.amity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.amity.adapter.ItemsAdapter
import com.app.amity.model.DataItem
import com.app.amity.presenter.PresenterData
import org.koin.core.KoinComponent

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment(), ItemsAdapter.OnItemClickListener, KoinComponent {


    private val dataItemList = MutableLiveData<List<DataItem>>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init(view)
    }

    private lateinit var mAdapter: ItemsAdapter

    private fun init(view: View) {
        var mRecyclerView = view.findViewById<RecyclerView>(R.id.rv)
        val layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        layoutManager.scrollToPosition(0)
        mRecyclerView.layoutManager = layoutManager
        mAdapter = ItemsAdapter(this)
        mRecyclerView.adapter = mAdapter
        dataItemList.observe(requireActivity(), Observer {
            it?.let {
                mAdapter.setData(it)
            }
        })

        PresenterData(dataItemList)
    }


    override fun onItemClick(pos: DataItem?) {

        findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
    }


}