package com.app.amity.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "dataitem")
data class DataItem(
    val completed: Boolean,
    @PrimaryKey
    val id: Int,
    val title: String,
    val userId: Int
)