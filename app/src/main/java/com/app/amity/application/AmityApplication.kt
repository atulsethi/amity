package com.app.amity.application

import android.app.Application
import android.util.Log
import com.app.amity.database.mDatabaseModule
import com.app.amity.restClient.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AmityApplication : Application() {
    init {
        Log.i(
            "Chat  ", ".\n" +
                    "+--------------------------------------------------------+\n" +
                    "|                  App is starting...                    |\n" +
                    "+--------------------------------------------------------+"
        )
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@AmityApplication)
            modules(networkModule)
            modules(mDatabaseModule)
//            modules(appModule)
        }
    }


}


